
FROM openjdk:8-jdk-alpine
ARG WAR_FILE=target/ummWeb.war
WORKDIR /opt/jenkins/workspace/Docker_deploy
COPY ${WAR_FILE} ummWeb.war
EXPOSE 8085
ENTRYPOINT [ "java", "-jar", "ummWeb.war" ]

